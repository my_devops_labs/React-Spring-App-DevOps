FROM alpine:3
RUN mkdir /app
COPY ./react-frontend /app/
WORKDIR /app/
RUN apk add --no-cache --update npm
RUN npm install
ENTRYPOINT ["npm"]
CMD ["start"]
